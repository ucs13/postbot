﻿namespace PostBot
{
    public class Post
    {
        public int id;
        public string link;
        private int hash;
        public string description;
        public string htmlLink;
        public User user;

        public string HtmlLink
        {
            get { return htmlLink; }
            set { htmlLink = @"<a href=""" + link + @""">" + value + "</a>"; }
        }

        public void SetHash()
        {            
            hash = link.GetHashCode(); 
        }

        public int GetHash()
        {
            return hash;
        }
    }
}