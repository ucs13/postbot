﻿using System.IO;
using Newtonsoft.Json;

namespace PostBot
{
    public class Config
    {
        public string host;
        public string bot_token;
        public string url;
        public string db_name;
        public string title;
        public long id_channel;
        public int interval;

        private static Config config;

        public static Config GetConfig()
        {
            if (config == null)
            {
                CreateConfig();
                return config;
            }
            else
            {
                return config;
            }
        }

        private static void CreateConfig()
        {
            var filename = @"./config_post_bot.json";
            try
            {
                using (StreamReader r = new StreamReader(filename))
                {
                    string json = r.ReadToEnd();
                    config = JsonConvert.DeserializeObject<Config>(json);
                }
                config.url = config.host + "bot" + config.bot_token + "/";
            }
            catch
            {
                config = null;
            }
        }
    }
}