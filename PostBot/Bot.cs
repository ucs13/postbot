﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PostBot
{
    public class Bot
    {
        private Config config;
        private bool getUpdate;
        private BackgroundWorker backgroundWorker;
        private Dictionary<long, Post> addingPost;
        private DataBase db;
        public Bot()
        {
            config = Config.GetConfig();
            db = new DataBase();
            backgroundWorker = new BackgroundWorker();
            addingPost = new Dictionary<long, Post>();
        }

        public void StartGettingUpdates()
        {
            getUpdate = true;
            backgroundWorker.DoWork += GetUpdates;
            backgroundWorker.RunWorkerAsync();
            Logger.Info("Start bot");
        }

        private void GetUpdates(object sender, DoWorkEventArgs e)        
        {
            long offset = 0;
            string json = default;
            Update update = null;
            bool res = default;            
            string url = config.url + "getUpdates?offset=";
            while (getUpdate)
            {
                json = Web.DownloadData(url + offset.ToString());
                update = JsonConvert.DeserializeObject<Update>(json);
                foreach (Result result in update.result)
                {
                    res = ProcessingResult(result);
                    if (res)
                    {
                        offset = result.update_id + 1;
                    }
                }
            }
        }

        private bool ProcessingResult(Result result)
        {
            bool res = false;
            Message message = result.message;
            if (message != null)
            {
                if ((message.chat.Type == ChatType.Private && addingPost.ContainsKey(message.chat.id)) || (!addingPost.ContainsKey(message.chat.id) && message.text == "/add"))
                {
                    res = AddPost(message);
                    if (res && !addingPost.ContainsKey(message.chat.id))
                    {
                        addingPost.Add(message.chat.id, null);
                    }                    
                }
            }
            else
            {
                res = true;
            }
            
            return res;
        }

        private bool AddPost(Message message)
        {
            Logger.Info("user {0} add post", message.from.GetFullName);
            bool res = false;
            Message msg = null;

            if (!addingPost.ContainsKey(message.chat.id))
            {
                msg = new Message
                {
                    chat = new Chat { id = message.chat.id },
                    text = "Отправь мне ссылку на статью"
                };
                
            }
            else if (addingPost[message.chat.id] == null)
            {
                if (message.entities != null && message.entities[0].Type == MessageEntityType.Url)
                {
                    Post post = new Post
                    {
                        link = message.text
                    };

                    post.SetHash();

                    bool result = db.CheckPostByHash(post.GetHash());
                    if (result)
                    {
                        msg = new Message
                        {
                            chat = new Chat { id = message.chat.id },
                            text = "Такую ссылку уже постили в канал, попробуй другую"
                        };
                    }
                    else
                    {
                        addingPost[message.chat.id] = post;
                        msg = new Message
                        {
                            chat = new Chat { id = message.chat.id },
                            text = "Отправь мне комментарий к статье или введи .(точку), чтобы без описания"
                        };
                    }
                }
                else
                {
                    msg = new Message
                    {
                        chat = new Chat { id = message.chat.id },
                        text = "Не похоже на ссылку, попробуй еще раз"
                    };
                }
            }
            else if (addingPost[message.chat.id] != null)
            {
                Post post = addingPost[message.chat.id];
                if (message.text.Length > 1)
                {
                    post.description = message.text;
                }
                post.HtmlLink = config.title;
                post.user = message.from;

                msg = new Message
                {
                    chat = new Chat { id = message.chat.id },
                    text = "Отлично, скоро отправлю твою статью в канал! Смотри мне, если будут ее минусовать - тебе будет больно.."
                };
                db.InsertPost(post);
                addingPost.Remove(message.chat.id);
            }

            res = SendMessage(msg);
            return res;
        }

        private bool SendMessage(Message message, string parseMode = null)
        {
            try
            {
                string url = config.url + "sendMessage?chat_id=" + message.chat.id + "&text=" + message.text;
                if (!String.IsNullOrEmpty(parseMode))
                {
                    url += "&parse_mode=" + parseMode;
                }
                string json = Web.DownloadData(url);
                //Update update = JsonConvert.DeserializeObject<Update>(json);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void FinishGettingUpdates()
        {
            getUpdate = false;
            backgroundWorker.CancelAsync();
            backgroundWorker.Dispose();
        }

        public void SendNextPost()
        {
            Logger.Debug("start send post in " + config.id_channel.ToString());

            List<Post> list = db.GetPost();

            if (list.Count > 0)
            {
                foreach (Post post in list)
                {
                    Message message = new Message
                    {
                        chat = new Chat { id = config.id_channel },
                        text = post.description + "\n" + post.HtmlLink
                    };

                    SendMessage(message, "HTML");
                    db.SetSendPost(post);
                }
            }
            list = null;
        }
    }
}