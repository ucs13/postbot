﻿using System;
using System.Timers;

namespace PostBot
{
    class Program
    {
        private Timer timer;
        private Config config;
        private Bot bot;

        static void Main(string[] args)
        {
            Program program = new Program();
            program.config = Config.GetConfig();
            program.bot = new Bot();
            program.bot.StartGettingUpdates();
            program.CreateTimer();
            program.Tick(null, null);
            while (true)
            {
                var exit = Console.ReadLine();
                if (exit == "exit")
                {
                    program.bot.FinishGettingUpdates();
                    Environment.Exit(0);
                }
            }
        }

        private void CreateTimer()
        {
            Logger.Info("start with interval " + config.interval);
            Logger.Info("min == " + (config.interval / 60000).ToString());
            timer = new Timer()
            {
                Interval = config.interval
            };
            timer.Elapsed += Tick;
            timer.Start();
        }

        private void Tick(Object source, ElapsedEventArgs e)
        {
            Logger.Info("tick");
            bot.SendNextPost();
        }
    }
}