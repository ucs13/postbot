﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using System.Threading;

namespace PostBot
{
    class DataBase
    {
        private SQLiteConnection connection;
        private SemaphoreSlim _semaphoreSlim;
        private Config config;

        internal DataBase()
        {
            config = Config.GetConfig();
            connection = new SQLiteConnection(string.Format("Data Source={0};", config.db_name));
            _semaphoreSlim = new SemaphoreSlim(1);
        }

        private long Query(string sql, Hashtable parameters, Action<SQLiteDataReader> processor)
        {
            _semaphoreSlim.Wait();
            long result = 0;
            Logger.Info("query db\n" + sql);
            try
            {
                connection.Open();
                SQLiteCommand cmd = new SQLiteCommand(sql, connection);

                if (parameters != null)
                {
                    var parametersName = parameters.Keys;
                    foreach (var name in parametersName)
                    {
                        Type t = typeof(int);
                        if (parameters[name] != null)
                            t = parameters[name].GetType();

                        if (t.Equals(typeof(string)))
                        {
                            byte[] UTF8bytes = UTF8Encoding.UTF8.GetBytes(parameters[name].ToString());
                            cmd.Parameters.AddWithValue(name.ToString(), UTF8bytes);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue(name.ToString(), parameters[name]);
                        }
                    }
                }

                if (processor == null)
                {
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        processor(reader);
                        result = reader.RecordsAffected;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                result = -1;
            }
            finally
            {
                connection.Close();
            }
            _semaphoreSlim.Release();
            return result;
        }

        internal bool CheckPostByHash(int hash)
        {
            Logger.Info("check post in base");

            int id = default;

            Hashtable parameters = new Hashtable
            {
                { "@hash", hash },
            };

            Query("select id from posts where hash = @hash limit 1", parameters, (SQLiteDataReader reader) =>
            {
                while (reader.Read())
                {
                    id = reader.GetInt32(0);
                }
            });

            if (id == default)
                return false;
            else
                return true;
        }

        internal void InsertPost(Post post)
        {
            Logger.Info("insert post in base");

            Hashtable parameters = new Hashtable
            {
                { "@link", post.link },
                { "@description", post.description },
                { "@html_link", post.HtmlLink },
                { "@hash", post.GetHash() },
                { "@user_id", post.user.id },
            };

            Query("insert into posts (link, description, html_link, hash, is_send, user_id) select @link, @description, @html_link, @hash, 0, @user_id", parameters, null);            
        }

        internal List<Post> GetPost()
        {
            List<Post> list = new List<Post>();
            Logger.Info("select post from base");
            Query("select ifnull(description, ''), html_link, id from posts where is_send = 0 ORDER BY RANDOM() limit 1", null, (SQLiteDataReader reader) =>
            {
                while (reader.Read())
                {
                    Post post = new Post
                    {
                        description = reader.GetString(0),
                        htmlLink = reader.GetString(1),
                        id = reader.GetInt32(2)
                    };
                    list.Add(post);
                }
            });
            return list;
        }

        internal void SetSendPost(Post post)
        {
            Logger.Info("set sent = 1 post from base");
            Hashtable parameters = new Hashtable
            {
                { "@id", post.id },
            };
            Query("update posts set is_send = 1 where id = @id", parameters, null);
        }
    }
}